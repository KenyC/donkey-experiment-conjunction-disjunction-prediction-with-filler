# %%
import enum
import os


class Color(enum.Enum):
    RED   = 1
    GREEN = 2
    BLUE  = 3

class Shape(enum.Enum):
    CIRCLE   = 1
    TRIANGLE = 2
    SQUARE   = 3

class VertPos(enum.Enum):
    TOP    = 0
    BOTTOM = 1


class HorizPos(enum.Enum):
    LEFT  = 0
    RIGHT = 1


n_shapes = 4

# %%
import argparse

parser = argparse.ArgumentParser(description="Creates image and csv for trials")
parser.add_argument("--dir", help='Path to the PCIbex directory')
args = parser.parse_args()

main_dir = "./"
if args.dir:
	main_dir = args.dir
chunk_includes_dir = os.path.join(main_dir, "chunk_includes")
data_includes_dir = os.path.join(main_dir, "data_includes")

# %%
"""
Poorman's constraint programming
"""


class Trial:
	def __init__(self):
		self.generate()

	def find_solution(self):
		while not self.constraint():
			self.generate()
		return self

	def generate(self):
		pass

	def constraint(self):
		return True


class TargetTrial(Trial):
	def generate(self):
		self.sentence_shape = random.choice(list(Shape))
		self.sentence_color = random.choice(list(Color))

		self.shapes = [random.choice(list(Shape)) for _ in range(n_shapes)]
		self.colors = [random.choice(list(Color)) for _ in range(n_shapes)]

	def pretty_print(self):
		print(self.sentence())
		print(f"Color: {self.sentence_color.name}")
		print(f"Shape: {self.sentence_shape.name}")
		self.pic_pretty_print()

	def sentence(self):
		return f"There is a {self.sentence_shape.name.lower()} and it is {self.sentence_color.name.lower()}."

	def pic_pretty_print(self):
		positions = [
			"top left",
			"bottom left",
			"top right",
			"bottom right"
		]
		for (i, (color, shape)) in enumerate(zip(self.colors, self.shapes)):
			print(f"{i}: {color.name} {shape.name} <= {positions[i]}")

	def color_shape_at(self, horiz : HorizPos, vert : VertPos) -> (Shape, Color):
		index = vert.value + horiz.value * 2
		return (self.shapes[index], self.colors[index])

class TargetDisjTrial(TargetTrial):
	def sentence(self):
		return f"Either there isn't a {self.sentence_shape.name.lower()} or it is {self.sentence_color.name.lower()}."




class ControlTrial(TargetTrial):
	def generate(self):
		super(ControlTrial, self).generate()
		self.sentence_other_shape = random.choice([shape for shape in Shape if shape != self.sentence_shape])

	def sentence(self):
		return f"There is a {self.sentence_shape.name.lower()} and the {self.sentence_other_shape.name.lower()} is {self.sentence_color.name.lower()}."


	def pretty_print(self):
		print(self.sentence())
		print(f"Color: {self.sentence_color.name}")
		print(f"Shape: {self.sentence_shape.name}")
		print(f"Other shape: {self.sentence_other_shape.name}")
		self.pic_pretty_print()





class TwoShapePos(TargetTrial):
	def generate(self):
		super(TwoShapePos, self).generate()
		self.sentence_shape = random.choice(list(Shape))
		self.sentence_other_shape = random.choice([shape for shape in Shape if shape != self.sentence_shape])

	def sentence(self):
		raise Exception("Abstract class")


	def pretty_print(self):
		print(self.sentence())
		print(f"Shape: {self.sentence_shape.name}")
		print(f"Other shape: {self.sentence_other_shape.name}")
		self.pic_pretty_print()



class ConjNoProCorner(TargetTrial):
	def generate(self):
		super(ConjNoProCorner, self).generate()
		self.sentence_shape = random.choice(list(Shape))
		self.horiz_pos = random.choice(list(HorizPos)) 
		self.vert_pos  = random.choice(list(VertPos))

	def sentence(self):
		return f"There is a {self.sentence_shape.name.lower()} and the {self.vert_pos.name.lower()} {self.horiz_pos.name.lower()} shape is {self.sentence_color.name.lower()}"


	def pretty_print(self):
		print(self.sentence())
		print(f"Shape:       {self.sentence_shape.name}")
		print(f"Color:       {self.sentence_color.name}")
		print(f"Horiz:       {self.horiz_pos.name}")
		print(f"Vert:        {self.vert_pos.name}")
		self.pic_pretty_print()




class DisjNoPro(TwoShapePos):
	def sentence(self):
		return f"Either there isn't a {self.sentence_shape.name.lower()} or there isn't a {self.sentence_other_shape.name.lower()}"

class ConjNoProFiller(TwoShapePos):
	def sentence(self):
		return f"There is a {self.sentence_shape.name.lower()} and there is a {self.sentence_other_shape.name.lower()}"



# %%
import random



class TargetTrialConjConj1False(TargetTrial):
	def constraint(model):
		"""
		Condition CONJ-CONJ1FALSE
		  - there is no SHAPE
		"""
		no_shape = all(
			shape != model.sentence_shape
			for shape in model.shapes
		)
		return no_shape


class TargetTrialConjConj2False(TargetTrial):
	def constraint(model):
		"""
		Condition CONJ-CONJ2FALSE
		  - there is only one SHAPE
		  - no SHAPE is COLOR
		"""
		one_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 1
		all_shapes_are_color = all(
			color != model.sentence_color
			for (shape, color) in zip(model.shapes, model.colors)
			if shape == model.sentence_shape
		)
		return one_shape and all_shapes_are_color

class TargetTrialConjExists(TargetTrial):
	def constraint(model):
		"""
		Condition CONJ-EXISTS
		  - there are two SHAPE
		  - only one SHAPE is COLOR
		"""
		two_shapes = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 2
		one_shape_is_color = sum(
			1
			for (shape, color) in zip(model.shapes, model.colors)
			if shape == model.sentence_shape and color != model.sentence_color
		) == 1
		return two_shapes and one_shape_is_color


class TargetTrialConjAll(TargetTrial):
	def constraint(model):
		"""
		Condition CONJ-ALL
		  - there are two SHAPE
		  - both are COLOR
		"""
		two_shapes = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 2
		two_shape_is_color = sum(
			1
			for (shape, color) in zip(model.shapes, model.colors)
			if shape == model.sentence_shape and color == model.sentence_color
		) == 2
		return two_shapes and two_shape_is_color

class TargetTrialConjUnique(TargetTrial):
	def constraint(model):
		"""
		Condition CONJ-UNIQUE
		  - there are two SHAPE
		  - both are COLOR
		"""
		one_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 1
		one_shape_is_color = sum(
			1
			for (shape, color) in zip(model.shapes, model.colors)
			if shape == model.sentence_shape and color == model.sentence_color
		) == 1
		return one_shape and one_shape_is_color


class TargetTrialDisjFalse(TargetDisjTrial):
	def constraint(model):
		"""
		Condition DISJ-FALSE
		  - there is one SHAPE
		  - it is not COLOR
		"""
		one_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 1
		zero_shape_is_color = sum(
			1
			for (shape, color) in zip(model.shapes, model.colors)
			if shape == model.sentence_shape and color == model.sentence_color
		) == 0
		return one_shape and zero_shape_is_color

class TargetTrialDisjExists(TargetDisjTrial):
	def constraint(model):
		"""
		Condition DISJ-EXISTS
		  - there are two SHAPE
		  - only one SHAPE is COLOR
		"""
		two_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 2
		one_shape_is_color = sum(
			1
			for (shape, color) in zip(model.shapes, model.colors)
			if shape == model.sentence_shape and color == model.sentence_color
		) == 1
		return two_shape and one_shape_is_color

class TargetTrialDisjAll(TargetDisjTrial):
	def constraint(model):
		"""
		Condition DISJ-ALL
		  - there are two SHAPE
		  - two SHAPE is COLOR
		"""
		two_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 2
		two_shape_is_color = sum(
			1
			for (shape, color) in zip(model.shapes, model.colors)
			if shape == model.sentence_shape and color == model.sentence_color
		) == 2
		return two_shape and two_shape_is_color

class TargetTrialDisjUnique(TargetDisjTrial):
	def constraint(model):
		"""
		Condition DISJ-UNIQUE
		  - there are two SHAPE
		  - two SHAPE is COLOR
		"""
		one_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 1
		one_shape_is_color = sum(
			1
			for (shape, color) in zip(model.shapes, model.colors)
			if shape == model.sentence_shape and color == model.sentence_color
		) == 1
		return one_shape and one_shape_is_color

class TargetTrialDisjTrueFirst(TargetDisjTrial):
	def constraint(model):
		"""
		Condition DISJ-TRUEFIRST
		  - there are no SHAPE
		"""
		zero_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 0
		return zero_shape


class ConjNoProCornerFalseFirst(ConjNoProCorner):
	def constraint(model):
		"""
		Condition CONJNOPROCORNER-F1st
		  - there is no SHAPE
		  - VPOS HPOS is not SHAPE
		  - VPOS HPOS is COLOR
		"""
		zero_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 0
		(shape, color) = model.color_shape_at(model.horiz_pos, model.vert_pos)
		return zero_shape and (color == model.sentence_color) and (shape != model.sentence_shape)
		

class ConjNoProCornerTrueWeak(ConjNoProCorner):
	def constraint(model):
		"""
		Condition CONJNOPROCORNER-TRUEWEAK
		  - there are two SHAPE
		  - VPOS HPOS is OTHERSHAPE
		  - VPOS HPOS is COLOR
		"""
		two_shapes = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 2
		(shape, color) = model.color_shape_at(model.horiz_pos, model.vert_pos)
		return two_shapes and (color == model.sentence_color) and (shape != model.sentence_shape)
		
class ConjNoProCornerTrueStrong(ConjNoProCorner):
	def constraint(model):
		"""
		Condition CONJNOPROCORNER-TRUEWEAK
		  - there is one SHAPE
		  - VPOS HPOS is OTHERSHAPE
		  - VPOS HPOS is COLOR
		"""
		one_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 1
		(shape, color) = model.color_shape_at(model.horiz_pos, model.vert_pos)
		return one_shape and (color == model.sentence_color) and (shape != model.sentence_shape)
		

class DisjNoProTrueFirst(DisjNoPro):
	def constraint(model):
		"""
		Condition DISJNOPRO-TRUE1ST
		  - there is zero SHAPE
		  - there is one OTHERSHAPE
		"""
		zero_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 0
		one_other_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_other_shape
		) == 1
		return zero_shape and one_other_shape
		

class DisjNoProFalse(DisjNoPro):
	def constraint(model):
		"""
		Condition DISJNOPRO-TRUE1ST
		  - there is one SHAPE
		  - there is one OTHERSHAPE
		"""
		one_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 1
		one_other_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_other_shape
		) == 1
		return one_shape and one_other_shape
		
class ConjNoProFillerFalseBoth(ConjNoProFiller):
	def constraint(model):
		"""
		Condition CONJNOPRO-FALSEBOTH
		  - there is zero SHAPE
		  - there is zero OTHERSHAPE
		"""
		zero_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 0
		zero_other_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_other_shape
		) == 0
		return zero_shape and zero_other_shape
		
class ConjNoProFillerFalseFirst(ConjNoProFiller):
	def constraint(model):
		"""
		Condition CONJNOPRO-FALSEFIRST
		  - there is zero SHAPE
		  - there is one OTHERSHAPE
		"""
		zero_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 0
		one_other_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_other_shape
		) == 1
		return zero_shape and one_other_shape
		

		
class ConjNoProFillerFalseSecond(ConjNoProFiller):
	def constraint(model):
		"""
		Condition CONJNOPRO-FALSESECOND
		  - there is one SHAPE
		  - there is zero OTHERSHAPE
		"""
		one_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 1
		zero_other_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_other_shape
		) == 0
		return one_shape and zero_other_shape
		

		
class ConjNoProFillerTrueBoth(ConjNoProFiller):
	def constraint(model):
		"""
		Condition CONJNOPRO-TRUEBOTH
		  - there is one SHAPE
		  - there is one OTHERSHAPE
		"""
		one_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_shape
		) == 1
		one_other_shape = sum(
			1
			for shape in model.shapes
			if shape == model.sentence_other_shape
		) == 1
		return one_shape and one_other_shape
		

# %%
"""
Check whether the conditions are well coded
"""

test_trials = [
	ConjNoProCornerTrueWeak,
	ConjNoProCornerTrueStrong,

	ConjNoProFillerFalseBoth,
	ConjNoProFillerFalseFirst,
	ConjNoProFillerFalseSecond,
	ConjNoProFillerTrueBoth,

	DisjNoProFalse,
	DisjNoProTrueFirst,
]

random.seed(12909052)

for GivenTrial in test_trials:
	print("========================")
	print(GivenTrial.__name__)
	print()
	for _ in range(1):
		model = GivenTrial().find_solution()
		model.pretty_print()
		# import pdb; pdb.set_trace()
		print()


# %%
model.pretty_print()
model.color_shape_at(HorizPos.RIGHT, VertPos.TOP)

# %%
import cairo

color_mapping = {
    Color.RED: (1., 0., 0.),
    Color.GREEN: (0., 1., 0.),
    Color.BLUE: (0., 0., 1.)
}

def draw_circle(context, color):
    context.set_source_rgb(*color_mapping[color])
    context.arc(0, 0.1 * 50, 0.9 * 50, 0, 2 * 3.14159)
    context.fill()

def draw_triangle(context, color):
    context.set_source_rgb(*color_mapping[color])
    SIDE_TRIANGLE = 100
    context.move_to(0, SIDE_TRIANGLE * (1 /2 -  3 ** 0.5 / 2))
    context.line_to(- SIDE_TRIANGLE / 2, SIDE_TRIANGLE / 2)
    context.line_to(  SIDE_TRIANGLE / 2, SIDE_TRIANGLE / 2)
    context.close_path()
    context.fill()

def draw_square(context, color):
    context.set_source_rgb(*color_mapping[color])
    context.rectangle(-50, -50, 100, 100)
    context.fill()

# Example usage:

shape_mapping = {
	Shape.CIRCLE   : draw_circle,
	Shape.SQUARE   : draw_square,
	Shape.TRIANGLE : draw_triangle,
}

def draw_model(model, filepath):
	SQUARE_SIZE = 120


	surface = cairo.SVGSurface(filepath, 2 * SQUARE_SIZE, 2 * SQUARE_SIZE)
	context = cairo.Context(surface)

	centers = [
		(SQUARE_SIZE / 2,     SQUARE_SIZE / 2),
		(SQUARE_SIZE / 2,     SQUARE_SIZE * 3 / 2),
		(SQUARE_SIZE * 3 / 2, SQUARE_SIZE / 2),
		(SQUARE_SIZE * 3 / 2, SQUARE_SIZE * 3 / 2),
	]

	for (color, shape, center) in zip(model.colors, model.shapes, centers):
		context.save()
		context.translate(*center)  # Centering the shapes
		shape_mapping[shape](context, color)
		context.restore()


	surface.finish()


# %%
"""
First, let's generate an example picture
"""
import os
import io

class ExampleTrial(TargetTrial):
	def constraint(self):
		return not any(color == Color.RED and shape == Shape.SQUARE for (color, shape) in zip(self.colors, self.shapes))

model = ExampleTrial().find_solution()


svg_resources_dict = {}
svg_buffer = io.BytesIO()
draw_model(model, svg_buffer)
svg_resources_dict["example.svg"] = svg_buffer.getvalue().decode("utf-8")


# %%
import csv
import json

trials = [
	(3, "conj", "pro",     "conj1false",    TargetTrialConjConj1False),
	(3, "conj", "pro",     "conj2false",    TargetTrialConjConj2False), 
	(3, "conj", "pro",     "exists",        TargetTrialConjExists), 
	(3, "conj", "pro",     "all",           TargetTrialConjAll), 
	(3, "conj", "pro",     "unique",        TargetTrialConjUnique),

	(3, "disj", "pro",     "false",         TargetTrialDisjFalse),
	(3, "disj", "pro",     "exists",        TargetTrialDisjExists), 
	(3, "disj", "pro",     "all",           TargetTrialDisjAll), 
	(3, "disj", "pro",     "unique",        TargetTrialDisjUnique),
	(3, "disj", "pro",     "truefirst",     TargetTrialDisjTrueFirst), 

	(3, "disj", "nopro",   "false",         DisjNoProFalse),
	(3, "disj", "nopro",   "truefirst",     DisjNoProTrueFirst), 

	(3, "conj", "nopro",   "falseboth",     ConjNoProFillerFalseBoth), 
	(3, "conj", "nopro",   "falsefirst",    ConjNoProFillerFalseFirst), 
	(3, "conj", "nopro",   "falsesecond",   ConjNoProFillerFalseSecond), 
	(3, "conj", "nopro",   "true",          ConjNoProFillerTrueBoth), 

	(3, "conj", "control",   "falsefirst",   ConjNoProCornerFalseFirst), 
	(3, "conj", "control",   "trueweak",     ConjNoProCornerTrueWeak), 
	(3, "conj", "control",   "truestrong",   ConjNoProCornerTrueStrong), 
]


trials_file_name = os.path.join(chunk_includes_dir, "trials.csv")
with open(trials_file_name, "w") as file:
	writer = csv.DictWriter(file, fieldnames=[
		"trial_no",

		"sentence",
		"connective",
		"set",
		"condition",
		"full_condition",

		"shape0", "shape1", "shape2", "shape3",
		"color0", "color1", "color2", "color3",

		"filename",
	])
	writer.writeheader()


	for n_trials, connective, group, condition, GivenTrial in trials:
		

		for i in range(n_trials):
			model = GivenTrial().find_solution()


			full_condition = f"{connective}_{group}_{condition}"
			filename = f"{full_condition}_{i}.svg"

			svg_buffer = io.BytesIO()
			draw_model(model, svg_buffer)
			# To inspect the images more easily, also render them to a file
			draw_model(model, os.path.join("out/", filename))
			svg_resources_dict[filename] = svg_buffer.getvalue().decode("utf-8")

			writer.writerow({
				"trial_no" : i,
				"sentence" : model.sentence(),


				"set" :            group,
				"connective" :     connective,
				"condition" :      condition,
				"full_condition" : full_condition,

				"shape0" : model.shapes[0].name, "shape1" : model.shapes[1].name, "shape2" : model.shapes[2].name, "shape3" : model.shapes[3].name,
				"color0" : model.colors[0].name, "color1" : model.colors[1].name, "color2" : model.colors[2].name, "color3" : model.colors[3].name,

				"filename" : filename,
			})
	print(f"Trials csv generated at {trials_file_name}")

svg_filepath = os.path.join(data_includes_dir, "_svg.js")
with open(svg_filepath, "w") as file:
	file.write(f"const SVGS = {json.dumps(svg_resources_dict)};")
	print(f"Wrote SVG inline at {svg_filepath}")


